import { put, takeLatest } from 'redux-saga/effects';
import { useNavigate } from 'react-router-dom';
import { postRequest, getRequest, postUnauthRequest, putRequest, putRequestFormData } from '../../helpers/api';
import { BASE_URL } from '../../constants/Urls';
import * as types from './types';

    function* getAllGroupes({payload}) {
        try {
          const req = yield getRequest(`${BASE_URL}/user/groupeAccount`);
          yield put({type: types.GETALL_GROUP_ACCOUNT_SUCCESS, payload: req});
        } catch (e) {
          yield put({type: types.GETALL_GROUP_ACCOUNT_FAILURE});
        }
      }
export default function* GroupSaga() {
  yield takeLatest(types.GETALL_GROUP_ACCOUNT_REQUEST, getAllGroupes);
}