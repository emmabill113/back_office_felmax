import * as types from './types';

const INITIAL_STATE = {
  loading: false,
  errorMsg: '',
  message: '',
  loggedMsg: '',
  status: null,
  groupe: [],
};

function GroupReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.GETALL_GROUP_ACCOUNT_REQUEST:
      return {
        ...state,
        errorMsg: '',
        message: '',
        status: null,
        loading: true
      };
    case types.GETALL_GROUP_ACCOUNT_FAILURE:
      return {
        ...state,
        loading: false,
        errorMsg: 'error',
        message: 'error',
        status: false,
      };
    case types.GETALL_GROUP_ACCOUNT_SUCCESS:
      console.log(action.payload, 'buuuuu')
      return {
        ...state,
        loading: false,
        errorMsg: '',
        status: true,
        groupe: action.payload,
      };
    default:
      return state;
  }
}

export default GroupReducer;