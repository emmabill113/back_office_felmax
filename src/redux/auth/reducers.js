import AsyncStorage from '@react-native-async-storage/async-storage';
import * as types from './types';

const INITIAL_STATE = {
  loading: false,
  token: null,
  tokenType: '',
  user: {},
  errorMsg: '',
  message: '',
  loggedMsg: '',
  status: null,
  statut: null,
  userInfo: null,
  transactions: null,
  recognizingEmail: false,
  updatingPassword: false,
  updatingMessage: null,
  recognizingEmailSuccess: false,
  verificationCode: null,
  step: 1,
  myProjects: [],
  cred: null,
  checkEmail: null,
  changePasswordSuccessfully: false,
  isContacting: false,
  contectStatus: false
};

function AuthReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.LOGIN_USER_REQUEST:
      return {
        ...state,
        errorMsg: '',
        message: '',
        status: null,
        loading: true,
      };
    case types.LOGIN_USER_FAILURE:
      return {
        ...state,
        loading: false,
        errorMsg: data,
        message: data,
        status: false,
      };
    case types.LOGIN_USER_SUCCESS:
      const data = action.payload;
      return {
        ...state,
        loading: false,
        errorMsg: '',
        loggedMsg: data.response.message,
        tokenType: data.response.token_type,
        token: data.response.access_token,
        status: data.statut,
        message: data.response.message
      };
    case types.SIGN_UP_REQUEST:
      return {
        ...state,
        loading: true,
        statut: null,
        status: null,
        message: '',
        errorMsg: '',
      };
    case types.SIGN_UP_FAILURE:
      return {
        ...state,
        loading: false,
        errorMsg: action?.payload,
        status: action?.payload?.status,
        message: '',
      };
    case types.SIGN_UP_SUCCESS:
      return {
        ...state,
        loading: false,
        message: action.payload.response.message,
        errorMsg: '',
        status: action.payload.status,
      };
    case types.LOCAL_LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        token: action.payload.access_token,
      };
    case types.LOGOUT_USER_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.LOGOUT_USER_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case types.LOGOUT_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        errorMsg: '',
        tokenType: '',
        token: null,
        userInfo: null
      };
    case types.GET_USER_PROFILE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.GET_USER_PROFILE_FAILED:
      return {
        ...state,
        loading: false,
      };
    case types.GET_USER_PROFILE_SUCCESS:
      const result = action.payload;
      const resultPorject = action.projects;

      console.log('ici se sont mes projets :', resultPorject)
      console.log('yyyoooooo',result)
      return {
        ...state,
        loading: false,
        userInfo: { ...result.user, balance: result.balance },
        transactions: result.transactions,
        myProjects: resultPorject,
      };
    case types.SET_PROFILE_PICTURE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.SET_PROFILE_PICTURE_SUCCESS:
      return {
        ...state,
        loading: false,
        userInfo: { ...state.userInfo, picture: action.payload.picture }
      };
    case types.SET_PROFILE_PICTURE_FAILED:
      return {
        ...state,
        loading: false,
        errorMsg: action.payload.error
      };
    case types.PROCESS_EMAIL_VERIFICATION:
      console.log('verif')
      return {
        ...state,
        recognizingEmail: true
      };
    case types.FAILED_EMAIL_VERIFICATION:
      console.log(action.payload)
      return {
        ...state,
        recognizingEmail: false,
        step: 1,
        checkEmail: action.payload.message
      };
    case types.SUCCESS_EMAIL_VERIFICATION:
      return {
        ...state,
        step: 2,
        recognizingEmail: false,
        recognizingEmailSuccess: true,
        verificationCode: action.payload.verificationCode
      };
    case types.CLEAR_VERIF_CODE:
      return {
        ...state,
        verificationCode: null
      }
    case types.CODE_CONFIRMED:
      return {
        ...state,
        step: 3
      }
    case types.CLEAR_ERROR_MESSAGE:
      console.log('clear')
      return {
        ...state,
        updatingMessage: null,
        checkEmail: null,
      }
    case types.UPDATE_PASSWORD:
      console.log('updating')
      return {
        ...state,
        updatingPassword: true,
      }
    case types.UPDATE_PASSWORD_FAILED:
      console.log("here ", action.payload.message);
      return {
        ...state,
        updatingPassword: false,
        updatingMessage: action.payload.message
      }
    case types.UPDATE_PASSWORD_SUCCESS:
      return {
        ...state,
        updatingPassword: false,
        updatingMessage: action.payload.message,
        cred: action.payload.cred,
        changePasswordSuccessfully: true,

      }
    case types.RESET_ALL_DATA_FIELDS:
      console.log('reset')
      return {
        ...state,
        step: 1,
        updatingMessage: null,
        updatingPassword: null,
        changePasswordSuccessfully: false,
        checkEmail: null
      }
    
    case types.RESET_ALL_ON_CONTACTER:
      return {
        ...state,
        contactMessage: null,
        contectStatus: false
      }
    default:
      return state;
  }
}

export default AuthReducer;