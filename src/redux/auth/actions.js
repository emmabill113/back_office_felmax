import * as types from './types';

export const login = payload => ({
  type: types.LOGIN_USER_REQUEST,
  payload,
});

export const localLogin = () => ({
  type: types.LOCAL_LOGIN_REQUEST,
});

export const logout = () => ({
  type: types.LOGOUT_USER_REQUEST,
});

export const getUserProfile = () => ({
  type: types.GET_USER_PROFILE_REQUEST,
});

export const processEmailRecognition = (payload) => ({
  type: types.PROCESS_EMAIL_VERIFICATION,
  payload
})

export const failedEmailVerification = () => ({
  type: types.FAILED_EMAIL_VERIFICATION,
})

export const successEmailVerification = () => ({
  type: types.SUCCESS_EMAIL_VERIFICATION,
})

export const clearVerificationCode = () => ({
  type: types.CLEAR_VERIF_CODE
})

export const codeConfirmed = () => {
  return {
    type: types.CODE_CONFIRMED
  }
}

export const updatePassword = (payload) => ({
  type: types.UPDATE_PASSWORD,
  payload
})

export const updatePasswordFailed = (payload) => ({
  type: types.UPDATE_PASSWORD_FAILED,
  payload
})

export const updatePasswordSuccess = (payload) => ({
  type: types.UPDATE_PASSWORD_SUCCESS,
  payload
})


export const setUserProfilePic = (payload) => ({
  type: types.SET_PROFILE_PICTURE_REQUEST,
  payload
});

export const clearMessage = () => {
  return {
    type: types.CLEAR_ERROR_MESSAGE,
  }
}

export const resetAllDataFields = () => {
  return {
    type: types.RESET_ALL_DATA_FIELDS,
  }
}
