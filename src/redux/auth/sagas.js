import { put, takeLatest } from 'redux-saga/effects';
import { useNavigate } from 'react-router-dom';
import { postRequest, getRequest, postUnauthRequest, putRequest, putRequestFormData } from '../../helpers/api';
import { BASE_URL } from '../../constants/Urls';
import * as types from './types';

function* login({ payload }) {
  let loginData = {
    email: payload.email,
    password: payload.password
  }
  try {
    const data = yield postUnauthRequest(`${BASE_URL}/user/login`, loginData);
    console.log(data)
    if (data.statut === true) {
      yield put({
        type: types.LOGIN_USER_SUCCESS,
        payload: data,
      });
      localStorage.setItem('@user', JSON.stringify(data.response));
      localStorage.setItem('@token', JSON.stringify(data.response.access_token));

      yield getUserProfile(data.response.access_token);
    } else {
      yield put({
        type: types.LOGIN_USER_FAILURE,
        payload: data?.error?.message || data?.errors[0]?.msg
      });
      console.log('dsdsdsdsds',data)
    }
  } catch (error) {
    console.error('Some Error: ', error);
    yield put({
      type: types.LOGIN_USER_FAILURE,
      payload: error?.message
    });
  }
}

function* localSignIn() {
  try {
    const data = yield localStorage.getItem('@user');
    console.log(data)
    if (!data) {
      //navigate('/login');
      return;
    }
    const response = JSON.parse(data);
    yield put({ type: types.LOCAL_LOGIN_SUCCESS, payload: response });
    // Get user informations
    yield getUserProfile();
  } catch (error) {
    console.error('error: ', error);
  }
}

function* logout() {
  localStorage.removeItem('@user');
  localStorage.removeItem('@token');
  yield put({ type: types.LOGOUT_USER_SUCCESS });
}

function* getUserProfile() {
  try {
    const data = yield getRequest(`${BASE_URL}/user/profile`);
    console.log("hey ", data)
    if (data.status === false) {
      yield put({ type: types.GET_USER_PROFILE_FAILED });
      localStorage.removeItem('@user');
      //rootNavigation.navigate(SCREENS.LOGIN);
      return;
    }
    yield put({ type: types.GET_USER_PROFILE_SUCCESS, payload: data.response.data, projects: data.response.projects});
   // rootNavigation.navigate(SCREENS.DRAWER);
  } catch (error) {
    console.error('Some Error on getProfile: ', error);
  }
}

function* processVerifEmail({ payload }) {
  const d = {
    email: payload.email
  }
  console.log(payload)
  try {
    const data = yield putRequest(`${BASE_URL}/user/reset-password`, JSON.stringify(d))
    console.log(data)
    if (data.status) {
      yield put({ type: types.SUCCESS_EMAIL_VERIFICATION, payload: { verificationCode: data.data.code } })
    } else {
      yield put({ type: types.FAILED_EMAIL_VERIFICATION, payload: { message: data.data.message } })
    }
  } catch (e) {
    console.log("-----------", typeof e.message)
    yield put({ type: types.FAILED_EMAIL_VERIFICATION, payload: { message: e.message } });
    console.log(e)
  }
}

function* changePassword({ payload }) {
  const d = {
    email: payload.email,
    password: payload.password
  }
  try {
    console.log(d)
    const data = yield putRequest(`${BASE_URL}/user/reset-password`, JSON.stringify(d))
    console.log("last", data)
    if (data.status) {
      yield put({ type: types.UPDATE_PASSWORD_SUCCESS, payload: { message: data.message, cred: d } })
      yield setTimeout(() => {
       // rootNavigation.navigate(SCREENS.LOGIN);
      }, 4000)
    } else {
      yield put({ type: types.UPDATE_PASSWORD_FAILED, payload: { message: data.error, status: false } })
    }
  } catch (e) {
    yield put({ type: types.UPDATE_PASSWORD_FAILED, payload: { message: e.message, status: false } })
  }
}

function* setUserProfilePicture({ payload }) {
  const formData = new FormData();
  formData.append('fichier', payload[0]);
  try {
    const d = yield putRequestFormData(`${BASE_URL}/user/add-profile-picture`, formData);
    console.log('Reached: ', d);
    if (d.status === true) {
      console.log('Reached 1');
      yield put({ type: types.SET_PROFILE_PICTURE_SUCCESS, payload: d.data });
      return;
    }
    console.log('Reached 2');
    yield put({ type: types.SET_PROFILE_PICTURE_FAILED, payload: d })
  } catch (error) {
    console.error('Something went wrong: ', error);
  }
}

export default function* AuthSaga() {
  yield takeLatest(types.LOGIN_USER_REQUEST, login);
  yield takeLatest(types.LOCAL_LOGIN_REQUEST, localSignIn);
  yield takeLatest(types.LOGOUT_USER_REQUEST, logout);
  yield takeLatest(types.GET_USER_PROFILE_REQUEST, getUserProfile);
  yield takeLatest(types.PROCESS_EMAIL_VERIFICATION, processVerifEmail);
  yield takeLatest(types.UPDATE_PASSWORD, changePassword);
  yield takeLatest(types.SET_PROFILE_PICTURE_REQUEST, setUserProfilePicture);
}