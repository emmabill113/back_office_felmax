import { combineReducers } from 'redux';
import AuthReducer from '../auth/reducers';
import GroupReducer from '../tontine/reducer';
/**
 * @description combine reducers
 */
const rootReducer = combineReducers({ AuthReducer, GroupReducer });

export default rootReducer;
