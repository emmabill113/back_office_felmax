import {applyMiddleware, createStore} from 'redux';
// import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from '@redux-saga/core';

import sagas from './sagas';
import rootReducer from './reducers';

// Saga middleware
const sagaMiddleWare = createSagaMiddleware();

export const store = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleWare),
);

// Run the saga
sagaMiddleWare.run(sagas);
