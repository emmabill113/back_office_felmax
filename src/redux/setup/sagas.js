import {all} from 'redux-saga/effects';
import AuthSaga from '../auth/sagas';
import GroupSaga from '../tontine/sagas';
/**
 * @description combine sagas
 */
export default function* Sagas() {
  yield all([AuthSaga(), GroupSaga()]);
}
