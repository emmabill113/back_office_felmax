import 'font-awesome/css/font-awesome.min.css';
import './assets/css/app.css';
import RoutesNavigation from './routes';


const App = () => {
    return (

        <RoutesNavigation />
    )
}

export default App;
