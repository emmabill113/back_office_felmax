import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import DashboardPage from '../pages/DashboardPage';
import TypographyPage from '../pages/TypographyPage';
import LoginPage from '../pages/auth/LoginPage';
import ResetPassword from '../pages/auth/ResetPassword';
import ProfilePage from '../pages/profile/ProfilePage';
import ChangePasswordPage from '../pages/profile/ChangePasswordPage';
import UserPreferencesPage from '../pages/profile/UserPreferencesPage';
import AdminBlankPage from '../pages/AdminBlankPage';
import ListUser from '../pages/utilisateurs/ListUser';
import Transaction from '../pages/Transaction';
import Tontine from '../pages/Tontine';
import { connect, useDispatch } from 'react-redux';

const RoutesNavigation = (userInfo, token) => {
    const [isLogin, setIslogin] = useState(null)
    useEffect(()=>{
        getToken()
    }, [userInfo]);

    const getToken = () => {
        let tokenSaved = localStorage.getItem('@token');
       
        if(tokenSaved){
            setIslogin(true);
            console.log('token exist', tokenSaved);
        }else{
            setIslogin(false);
            console.log("token doesn't exist", tokenSaved);
            
        }
    }

    const PageNotFound = () =>{
        return(
            <div style={{height:'100%', alignContent:'center', justifyContent:'center'}}>
                <h6 style={{textAlign:'center'}}>
                    error
                </h6>
                <h1 style={{textAlign:'center'}}>
                    Page not found !
                </h1>
            </div>
        )
    }

    return (
        <Router>
            {isLogin?
                <Routes>
                    <Route path="/" element={<DashboardPage />} />
                    <Route path="/reset-password" element={<ResetPassword />} />
                    <Route path="/profile" element={<ProfilePage />} />
                    <Route path="/change-password" element={<ChangePasswordPage />} />
                    <Route path="/preferences" element={<UserPreferencesPage />} />
                    <Route path="/typography" element={<TypographyPage />} />
                    <Route path="/blank-page" element={<AdminBlankPage />} />
                    <Route path="/list-user" element={<ListUser />} />
                    <Route path="/transaction" element={<Transaction />} />
                    <Route path="/tontine" element={<Tontine />} />
                    <Route path='*'  element={<PageNotFound/> }/>
                </Routes>
                :
                <Routes>
                    <Route exact path="/" element={<LoginPage />} />
                    <Route path='*'  element={<PageNotFound/> }/>
                </Routes>
            }
        </Router>
    );
};

const mapStateToProps = ({ AuthReducer }) => ({
    userInfo: AuthReducer.userInfo,
    token: AuthReducer.token,

});
export default connect(mapStateToProps)(RoutesNavigation);
