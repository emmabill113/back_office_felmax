const colors = {
  thirdlyColor: '#D3D2E1',
  secondaryColor: '#7b7b7b',
  primaryColor: '#c70039',
  whiteColor: '#FFFFFF',
  whiteOpacityColor: "#ffffff8c",
  blackColor: '#000000',
  greyColor: '#9E9E9E',
  greenColor: '#4CAF50',
  blueDarkColor: '#023047',
  blackLightColor: '#354f52',
  blueFacebookColor: '#023e8a',
  grayLinkColor: '#e5e5e5',
  success: '#4CAF50',
  danger: '#f83a53',
  yellow: '#EABB13',
  alert: '#fc6203'
};

export default colors;
