import React, { useEffect, useState } from "react";
import { Container, Row, Col, Table, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import adminLayout from "../../hoc/adminLayout";
import { connect, useDispatch } from "react-redux";
import {GetAllGroup} from '../../redux/tontine/actions'

const ListTontine = ({groupe, userInfo}) => {
    const [can, setCan] = useState(false);
    console.log('ici tout les group', groupe)
    const dispatcher = useDispatch();
    const [searchTerm, setSearchTerm] = useState("");
    const [selectedTontine, setSelectedTontine] = useState(null);

    useEffect(() => {
        dispatcher(GetAllGroup());
        setTimeout(() => {
          setCan(true);
        }, 2000);
      }, []);
      

    const [tontines, setTontines] = useState([]);
    
    useEffect(()=>{
        setTontines(groupe);
    }, [groupe])


        const search =(value)=>{
            const currentItems = tontines
        .filter((item) => {
            if (value === "") {
                return item;
            } else if (
                item.name.toLowerCase().includes(value.toLowerCase())
            ) {
                return item;
            }
        })
        .slice(0, 5);
        setTontines(currentItems);
        }

    const [members, setMembers] = useState([]);

    const [transactions, setTransactions] = useState([]);

    const handleTontineSelect = (tontine) => {
        console.log('deee', tontine)
        setSelectedTontine(tontine);
        setMembers(tontine.members);
        setTransactions(tontine.account.transactions);

        console.log('sdsdsdsdsdsdsf', tontine.account, 'sfdfdfdfdfd');
    };

    const handleTontineDelete = (tontineId) => {
        const updatedTontines = tontines.filter((tontine) => tontine._id !== tontineId);
        setTontines(updatedTontines);
    };

    const handleMemberDelete = (memberId) => {
        const updatedMembers = members.filter((member) => member.id !== memberId);
        const updatedTontines = tontines.map((tontine) => {
            if (tontine.id === selectedTontine.id) {
                return {
                    ...tontine,
                    members: updatedMembers
                };
            }
            return tontine;
        });
        setTontines(updatedTontines);
        setMembers(updatedMembers);
    };
    return (
        <Container fluid>
            <Row>
                <Col md={5}>
                    <h1 className="titre">Liste des Tontines</h1>
                    <div className="py-2 col-md-12">
                        <input
                            className="form-control"
                            type="text"
                            placeholder="Rechercher une tontine"
                            onChange={(e) => search(e.target.value)}
                        />
                    </div>
                </Col>
            </Row>
            <Row>
                <Col md={5}>
                    <Table bordered >
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nom</th>
                                <th>Type</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            {tontines.map((tontine, index) => (
                                <tr key={tontine._id}>
                                    <td>{index+1}</td>
                                    <td>{tontine.name}</td>
                                    <td>{tontine.accountTypes}</td>
                                    <td>
                                        <div className="row justify-content-center">
                                            <Link className="col " aria-hidden="true" onClick={() => handleTontineSelect(tontine)}>
                                                <i class="fa text-info fa-info-circle"></i>
                                            </Link>{" "}
                                            <Link className="col" onClick={() => handleTontineDelete(tontine._id)}>
                                                <i class="fa col text-danger fa-trash-o" aria-hidden="true" ></i>
                                            </Link>
                                        </div>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Col>
                <Col md={5}>
                    {selectedTontine ? (
                        <div>
                            <h2 className="titre">{selectedTontine.name}</h2>
                            <div className="row">
                                <p className="col">Date de début: {selectedTontine.startDate}</p>
                                <p className="col">date de fin: {selectedTontine.endDate}</p>
                            </div>
                            <div className="row">
                                <p className="col">Objectif: 30000 FCFA</p>
                                <p className="col">Solde: {selectedTontine.account.balance}</p>
                            </div>
                            <div className="row">
                                <p className="col">Membres: 10</p>
                                <p className="col">Fréquence : {selectedTontine.frequence} jours</p>
                            </div>
                            <h3 className="titre">Membres</h3>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Téléphone</th>
                                        <th>Ville</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {members?.map((member, ind) => (
                                        <tr key={member._id}>
                                            <td>{ind+1}</td>
                                            <td>{member.name}</td>
                                            <td>{member.surname}</td>
                                            <td>{member.phone}</td>
                                            <td>{member.city}</td>
                                            <td>
                                                <Link className=" " onClick={() => handleMemberDelete(member.id)}>
                                                    <i class="fa col text-danger fa-trash-o" aria-hidden="true" ></i>
                                                </Link>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>
                            <h3 className="titre">Historique des transactions</h3>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Transaction ID</th>
                                        {/* <th>Montant</th>
                                        <th>Date</th> */}
                                    </tr>
                                </thead>
                                <tbody>
                                    {transactions?.map((transaction, ind) => (
                                        <tr key={transaction._id}>
                                            <td>{ind+1}</td>
                                            <td>{transaction}</td>
                                            {/* <td>{transaction.balance}</td>
                                            <td>{transaction.date}</td> */}
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>
                        </div>
                    ) : (
                        <p className="titre">Sélectionnez une tontine pour voir ses détails.</p>
                    )}
                </Col>
            </Row>
        </Container>
    );
};

const mapStateToProps = ({GroupReducer, AuthReducer}) => ({
    groupe: GroupReducer.groupe,
    userInfo: AuthReducer.userInfo,
  });
  
export default connect(mapStateToProps)(adminLayout(ListTontine));
