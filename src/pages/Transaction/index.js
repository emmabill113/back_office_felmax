import React from "react";
import adminLayout from "../../hoc/adminLayout";
import "./index.css"
import { useState } from "react";
import Listitems from "../../common/listItems";
import CreateUser from "../../common/createUser";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
const Transaction = () => {

    const [isCreate, setIsCreate] = useState(true)
    const [myData, setMyData] = useState([]);

    const handleDelete = (id) => {
        setMyData(myData.filter((item) => item.id !== id));
    };

    const column = [  { name: "ID" },  { name: "SERVICE" },  { name: "OPÉRATEUR" },  { name: "MONTANT" },  { name: "TÉLÉPHONE" },  { name: "UTLISATEUR" }];


    const data = [
        {
            item1: "1",
            item2: "Enéo",
            item3: "Express Union",
            item4: '2000 FCFA',
            item5: "255454657",
            item6: "Donald",
        },
        {
            item1: "2",
            item2: "Canal plus",
            item3: "Express Union",
            item4: '1000 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "3",
            item2: "Camwater",
            item3: "Express Union",
            item4: '1500 FCFA',
            item5: "255454657",
            item6: "Arsène",
        },
        {
            item1: "4",
            item2: "Achat de crédit",
            item3: "MTN Cameroun",
            item4: '500 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "5",
            item2: "Enéo",
            item3: "Express Union",
            item4: '2000 FCFA',
            item5: "255454657",
            item6: "Donald",
        },
        {
            item1: "6",
            item2: "Canal plus",
            item3: "Express Union",
            item4: '1000 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "7",
            item2: "Camwater",
            item3: "Express Union",
            item4: '1500 FCFA',
            item5: "255454657",
            item6: "Arsène",
        },
        {
            item1: "8",
            item2: "Achat de crédit",
            item3: "MTN Cameroun",
            item4: '500 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "9",
            item2: "Enéo",
            item3: "Express Union",
            item4: '2000 FCFA',
            item5: "255454657",
            item6: "Donald",
        },
        {
            item1: "10",
            item2: "Canal plus",
            item3: "Express Union",
            item4: '1000 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "11",
            item2: "Camwater",
            item3: "Express Union",
            item4: '1500 FCFA',
            item5: "255454657",
            item6: "Arsène",
        },
        {
            item1: "12",
            item2: "Achat de crédit",
            item3: "MTN Cameroun",
            item4: '500 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "13",
            item2: "Enéo",
            item3: "Express Union",
            item4: '2000 FCFA',
            item5: "255454657",
            item6: "Donald",
        },
        {
            item1: "14",
            item2: "Canal plus",
            item3: "Express Union",
            item4: '1000 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "15",
            item2: "Camwater",
            item3: "Express Union",
            item4: '1500 FCFA',
            item5: "255454657",
            item6: "Arsène",
        },
        {
            item1: "16",
            item2: "Achat de crédit",
            item3: "MTN Cameroun",
            item4: '500 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "17",
            item2: "Enéo",
            item3: "Express Union",
            item4: '2000 FCFA',
            item5: "255454657",
            item6: "Donald",
        },
        {
            item1: "18",
            item2: "Canal plus",
            item3: "Express Union",
            item4: '1000 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "19",
            item2: "Camwater",
            item3: "Express Union",
            item4: '1500 FCFA',
            item5: "255454657",
            item6: "Arsène",
        },
        {
            item1: "20",
            item2: "Achat de crédit",
            item3: "MTN Cameroun",
            item4: '500 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "1",
            item2: "Enéo",
            item3: "Express Union",
            item4: '2000 FCFA',
            item5: "255454657",
            item6: "Donald",
        },
        {
            item1: "21",
            item2: "Canal plus",
            item3: "Express Union",
            item4: '1000 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "22",
            item2: "Camwater",
            item3: "Express Union",
            item4: '1500 FCFA',
            item5: "255454657",
            item6: "Arsène",
        },
        {
            item1: "23",
            item2: "Achat de crédit",
            item3: "MTN Cameroun",
            item4: '500 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "24",
            item2: "Enéo",
            item3: "Express Union",
            item4: '2000 FCFA',
            item5: "255454657",
            item6: "Donald",
        },
        {
            item1: "25",
            item2: "Canal plus",
            item3: "Express Union",
            item4: '1000 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "26",
            item2: "Camwater",
            item3: "Express Union",
            item4: '1500 FCFA',
            item5: "255454657",
            item6: "Arsène",
        },
        {
            item1: "27",
            item2: "Achat de crédit",
            item3: "MTN Cameroun",
            item4: '500 FCFA',
            item5: "2554545457",
            item6: "Emmanuel",
        },
        {
            item1: "28",
            item2: "Canl plus",
            item3: "Express Union",
            item4: '20000 FCFA',
            item5: "255454657",
            item6: "Donald",
        },
        {
            item1: "29",
            item2: "Préinscription",
            item3: "Express Union",
            item4: '10000 FCFA',
            item5: "2554545457",
            item6: "Tatiana",
        },
        {
            item1: "30",
            item2: "Camwater",
            item3: "Express Union",
            item4: '1500 FCFA',
            item5: "255454657",
            item6: "Joel",
        },
        
       
    ];

    const modalFooterContent = () => {
        return <>
            <div style={{ width: "100%" }}>
                <button className="btn btn-default">Save</button>
            </div>
        </>;
    }



    return (
        <>
            {isCreate ?
                <div className="table-container w-100">
                    <div className="row">
                        <div className="col">
                            <h6 className="pb-2 mb-0 titre">Toutes transactions</h6>
                        </div>
                        <div className="col-md-4 d-flex btnGroup">
                            <button onClick={() => setIsCreate(false)} className="btn btn-success rounded-pill low-height-btn mr-1">
                                Réussi
                            </button>
                            <button onClick={() => setIsCreate(false)} className="btn  rounded-pill  btn-warning low-height-btn mr-1">
                                Échouée
                            </button>
                            <button onClick={() => setIsCreate(false)} className="btn  rounded-pill  btn-info low-height-btn">
                                En attente
                            </button>
                        </div>
                    </div>
                    <div className="d-flex text-muted">
                        <Listitems columns={column} data={data} />
                    </div>
                </div> :
                <div className="table-container w-100">
                    <div className="row">
                        <div className="col">
                            <h6 className="pb-2 mb-0 titre">Transactions réussi</h6>
                        </div>
                        <div className="col-md-2 d-flex justify-content-end">
                            <button onClick={()=>setIsCreate(true)} className="btn btn-info rounded-pill low-height-btn mr-1">
                                Retour
                            </button>
                        </div>
                    </div>
                    <Listitems columns={column} data={data} />
                </div>
            }

        </>
    );
}

export default adminLayout(Transaction);