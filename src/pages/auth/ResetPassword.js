import React from "react";
import "../../assets/css/login.css"
import { Link } from 'react-router-dom';
import authLayout from "../../hoc/authLayout";
import colors from "../../constants/Colors"

class ResetPassword extends React.Component {
    constructor(props){
        super(props);

        this.state = {};
    }

    render(){
        return <>
            <div className="reset-password-section text-center">
                  <h3><i className="mt-4 fa fa-lock fa-3x"></i></h3>
                  <h2 className="small text-center">Mot de passe oublié ?</h2>
                  <p>Vous pouvez Réinitialiser votre mot de passe</p>
                  <div className="panel-body">
    
                    <form id="register-form" role="form" autocomplete="off" className="form" method="post">
    
                      <div className="form-group">
                        <span className="input-group-addon"><i className="glyphicon glyphicon-envelope color-blue"></i></span>
                            <input id="email" name="email" placeholder="email address" className="form-control form-control-lg"  type="email" />
                      </div>

                        <div className="form-group mt-2">
                            <button  type="button" className="btn btn-primary btn-sl">Réinitialiser</button>
                            <p className="small  mt-2 pt-1 mb-0">Vous souvenez-vous  de votre mot de passe ? <Link to="/"
                                className="link-danger small">Se connecter</Link></p>
                        </div>
                      
                    </form>
    
                  </div>
                </div>
        </>
    }
}

export default authLayout(ResetPassword);