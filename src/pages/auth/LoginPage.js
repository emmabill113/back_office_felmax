import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { connect, useDispatch } from 'react-redux';
 
import {
  login,
} from "../../redux/auth/actions"
import "./index.css"


const Login = ({ loading, errorMsg, status }) => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleLogin = async () => {
    try {
      if (email === "" || password === "") {
        setErrorMessage("Veillez renseigner tous les champs");
        return;
      }
      const payload = { email: email.trim(), password: password };
       dispatch(login(payload));
    } catch (error) {
      setErrorMessage(errorMsg);
    }
  };
  
  /*useEffect(() => {
    if (status) {
      navigate("/");
    }
  }, [status, navigate]);*/

  return (
    <div className="content d-flex justify-content-center align-items-center">
        <div className="col-md-4 p-2">
            <span className="titrerr"> FELSMAX MONEY, une réussite du 21e siècle !</span>
            <p className="titrer">connectez-vous et regarder la magie opérer</p>
        </div>
      <form className="login-form col-md-4">
        <div className="d-flex align-items-center my-4">
          <h1 className="text-center">Se connecter</h1>
        </div>
        <div className="form-outline mb-4">
          <label className="form-label" htmlFor="form3Example3">E-mail</label>
          <input type="email" id="form3Example3" className="form-control form-control-lg"
            placeholder="Entrer une adrresse mail valide"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
          />
        </div>
        <div className="form-outline mb-3">
          <label className="form-label" htmlFor="form3Example4">Mot de passe </label>
          <input type="password" id="form3Example4" className="form-control form-control-lg"
            placeholder="Entrer le mot de passe"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
        </div>
        <div>
          <p style={{ color: 'red' }}>{errorMessage ? errorMessage : ''}</p>
          <p style={{ color: 'red' }}>{errorMsg ? errorMsg : errorMsg}</p>
        </div>
        <div className="d-flex justify-content-between align-items-center">
          <div className="form-check mb-0">
            <input className="form-check-input me-2" type="checkbox" value="" id="form2Example3" />
            <label className="form-check-label rst-connet" htmlFor="form2Example3">
              Rester connecter
            </label>
          </div>
        </div>
        <div className="text-center w-100  mt-4 pt-2">
          <button  onClick={handleLogin} type="button" className="btn btn-primary rounded-pill">Connexion</button>
          <p className="small  mt-2 pt-1 mb-0 rst-connet">Vous n'avez pas de compte ? <a href="#!"
            className="link-danger">contacter l'administrateur</a></p>
        </div>
      </form>
      </div>
  );
};

const mapStateToProps = ({ AuthReducer }) => ({
  loading: AuthReducer.loading,
  errorMsg: AuthReducer.errorMsg,
  message: AuthReducer.message,
  status: AuthReducer.status,
  token: AuthReducer.token,
  cred: AuthReducer.cred,
});

export default connect(mapStateToProps)(Login);
