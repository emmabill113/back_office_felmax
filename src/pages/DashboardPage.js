import React from "react";
import adminLayout from "../hoc/adminLayout";
import { Link } from 'react-router-dom';
import Charts from "../common/chart";
import Card1 from "../common/card1";

import "./index.css"

class DashboardPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {}
  }

  render() {
    return <>
      <div className="row rowss ">
       <Card1 chiffre={5384} module={'Utilisateurs'}/>
       <Card1 chiffre={845324} module={'Transactions'}/>
       <Card1 chiffre={167} module={'Projets validés'}/>
       <Card1 chiffre={87} module={'Projets soummis'}/>
       <Card1 chiffre={8} module={'Projets rejétés'}/>
       <Card1 chiffre={143} module={'Groupes'}/>
        <Charts />
      </div>
    </>
  }
}

export default adminLayout(DashboardPage);