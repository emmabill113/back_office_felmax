import React from "react";
import "../../assets/css/profile.css"
import userProfileLayout from "../../hoc/userProfileLayout";

const ProfilePage =()=> {

        return( <>
            <div className="my-3 p-3 bg-body rounded shadow-sm">
                <h6 className="border-bottom pb-2 mb-0 mb-3">Informations presonnels</h6>
                <form className="">
                    <div className="row">
                        <div className="col">
                            <label htmlFor="exampleInputEmail1" className="form-label">Nom</label>
                            <div className="input-group mb-3">
                                <input type="text" className="form-control" placeholder="Username" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                            </div>
                        </div>
                        <div className="col">
                            <label htmlFor="exampleInputEmail1" className="form-label">Prenom</label>
                            <div className="input-group mb-3">
                                <input type="text" className="form-control" placeholder="Email Address" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <label htmlFor="exampleInputEmail1" className="form-label">e-mail</label>
                            <div className="input-group mb-3">
                                <input type="text" className="form-control" placeholder="First Name" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                            </div>
                        </div>
                        <div className="col">
                            <label htmlFor="exampleInputEmail1" className="form-label">Numéro de téléphone</label>
                            <div className="input-group mb-3">
                                <input type="text" className="form-control" placeholder="Last Name" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col">
                            <label htmlFor="exampleInputEmail1" className="form-label">e-mail</label>
                            <div className="input-group mb-3">
                                <input type="text" className="form-control" placeholder="First Name" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                            </div>
                        </div>
                        <div className="col">
                            <label htmlFor="exampleInputEmail1" className="form-label">Numéro de téléphone</label>
                            <div className="input-group mb-3">
                                <input type="text" className="form-control" placeholder="Last Name" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                            </div>
                        </div>
                    </div>
                        <button type="submit" className="btn btn-default">Modifier</button>
                </form>
            </div>

        </>
        );
}

export default userProfileLayout(ProfilePage);