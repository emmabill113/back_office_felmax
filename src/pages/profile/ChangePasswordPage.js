import React from "react";
import "../../assets/css/profile.css"
import userProfileLayout from "../../hoc/userProfileLayout";

class ProfilePage extends React.Component {
    constructor(props){
        super(props);

        this.state = {}
    }

    render(){
        return <>
                <div className="my-3 p-3 bg-body rounded shadow-sm">
                    <h6 className="border-bottom pb-2 mb-0 mb-3">Changer le mot de passe</h6>

                    <div className="row">
                        <div className="col-4">
                            <p>Votre mot de passe doit contenir</p>
                            <p> <i className="fa fa-check"></i>Au moins 8 caractères.</p>
                            <p> <i className="fa fa-check"></i>Au moins 1 chiffre.</p>
                            <p> <i className="fa fa-check"></i> Au moins 1 caractère spécial.</p>
                        </div>
                        <div className="col-8">
                            <form>
                                <div className="mb-3">
                                    <label htmlFor="exampleInputEmail1" className="form-label">Mot de passe actuel</label>
                                    <div className="input-group mb-3">
                                        <input type="password" className="form-control" placeholder="Mot de passe actuel" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="exampleInputEmail1" className="form-label">Nouveau mot de passe</label>
                                    <div className="input-group mb-3">
                                        <input type="password" className="form-control" placeholder="Nouveau mot de passe" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="exampleInputEmail1" className="form-label">Confirmer le nouveau mot de passe</label>
                                    <div className="input-group mb-3">
                                        <input type="password" className="form-control" placeholder="confirmer le nouveau mot de passe" aria-label="Recipient's username" aria-describedby="basic-addon2"/>
                                    </div>
                                </div>
                                <hr/>
                                <button type="submit" className="btn btn-default">Changer</button>
                            </form>
                        </div>
                    </div>
                </div>
            
        </>
    }
}

export default userProfileLayout(ProfilePage);