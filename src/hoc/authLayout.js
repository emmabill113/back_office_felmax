import React from "react";
import image from "../assets/images/weather_db.jpg";
import "./index.css";
const authLayout = (ChildComponent) => {
    class AuthLayout extends React.Component {
        constructor(props) {
            super(props);

            this.state = {};
        }

        render() {
            return <>
                <section className="vh-100">
                    <div className="container-fluid h-custom">
                        <div className="row d-flex align-items-center h-100">
                            <div className="col-md-12 col-lg-12 col-xl-6 h-100">
                                <img alt="hey" src={image} className="img-fluid" height={'100%'} style={{marginLeft:'-2%'}} />
                            </div>
                            <div className="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
                                <ChildComponent {...this.props} />
                            </div>
                        </div>
                    </div>
                </section>
            </>
        }
    }

    return AuthLayout;
}

export default authLayout;