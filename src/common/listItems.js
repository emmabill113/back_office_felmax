import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const ListItems = ({ data, setData, columns }) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(5);

  const [myData, setMyData] = useState([]);

    const handleDelete = (id) => {
      setMyData(myData.filter((item) => item.id !== id));
    };

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  /*const handleDelete = (id) => {
    setData(data.filter((item) => item.id !== id));
  };*/

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = data
    .filter((item) => {
      if (searchTerm === "") {
        return item;
      } else if (
        item.item1.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.item2.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.item3.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.item4.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.item5.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.item6.toLowerCase().includes(searchTerm.toLowerCase())
      ) {
        return item;
      }
    })
    .slice(indexOfFirstItem, indexOfLastItem);

  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(data.length / itemsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <div className="w-100">
      <div className="py-2 w-40">
        <input
          className="form-control"
          type="text"
          placeholder="Rechercher globale"
          onChange={(e) => setSearchTerm(e.target.value)}
        />
      </div>
      <table className="table">
        <thead>
          <tr>
            <th>
              <input type="checkbox" />
            </th>
            {columns.map((column, index) => (
              <th key={index}>{column.name}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {currentItems.map((item, index) => (
            <tr key={index}>
              <td>
                <input type="checkbox" />
              </td>
              <td>{item.item1}</td>
              <td>{item.item2}</td>
              <td>{item.item3}</td>
              <td>{item.item4}</td>
              <td>{item.item5}</td>
              <td>{item.item6}</td>
              <td>{item.item7}</td>
              <td>
                <div className="dropdown table-action-dropdown">
                  <button
                    className="btn  btn-sm dropdown-toggle"
                    type="button"
                    id={`dropdownMenuButtonSM-${index}`}
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    <i className="fa treedot fa-ellipsis-v" aria-hidden="true"></i>
                  </button>
                  <ul
                    className=" p-2 dropdown-menu text-small shadow"
                    aria-labelledby={`dropdownMenuButtonSM-${index}`}
                  >
                    <li>
                      <Link className="dropdown-item" to="#">
                        <i className="fa fa-sign-out" aria-hidden="true"></i> Désactiver
                      </Link>
                    </li>
                    <li>
                      <hr className="dropdown-divider" />
                    </li>
                    <li>
                      <Link className="dropdown-item" onClick={handleDelete}>
                        <i className="fa fa-trash-o" aria-hidden="true"></i> Supprimer
                      </Link>
                    </li>
                  </ul>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="d-flex justify-content-center">
        <div className="mx-2">
          <select
            className="form-select rounded-pill form-select-sm"
            aria-label=".form-select-sm example"
            value={itemsPerPage}
            onChange={(e) => setItemsPerPage(e.target.value)}
          >
            <option defaultValue>5</option>
            <option value="10">10</option>
            <option value="15">15</option>
            <option value="25">25</option>
            <option value="30">30</option>
            <option value="35">35</option>
            <option value="40">40</option>
            <option value="45">45</option>
            <option value="50">50</option>
          </select>
        </div>
        <div className="mx-2">
          <Button
            className="bg-light btn-sm border-0 text-info rounded-pill"
            variant="light"
            disabled={currentPage === 1}
            onClick={() => handlePageChange(currentPage - 1)}
          >
            Précédent
          </Button>
        </div>
        <div className="mx-2">
          <Button
            className="bg-light border-0 btn-sm border-info text-info rounded-pill"
            variant="light"
            disabled={currentPage === pageNumbers.length}
            onClick={() => handlePageChange(currentPage + 1)}
          >
            Suivant
          </Button>
        </div>
      </div>
    </div>
  );
};

export default ListItems;
