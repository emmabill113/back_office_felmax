import React from 'react';
import { LineChart, Line, BarChart, Cell, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, PieChart, Pie } from 'recharts';

const data = [
    { name: 'Enéo', ventes: 1000, color: '#0088FE' },
    { name: 'Canal+', ventes: 1500, color: '#00C49F' },
    { name: 'CDI', ventes: 800, color: '#FFBB28' },
    { name: 'Université', ventes: 2000, color: '#FF8042' },
    { name: 'Scolaire', ventes: 1200, color: '#AF19FF' },
];

const dataFactures = [
    { name: 'Orange', transaction: 20 },
    { name: 'MTN', transaction: 50 },
    { name: 'EU', transaction: 60 },
    { name: 'YOOMEE', transaction: 70 },
    { name: 'UBA', transaction: 80 },
];

const dataGroupe = [
    { name: 'Tontine', groupe: 20 },
    { name: 'Gage', groupe: 50 },
];
const dataProjet = [
    { secteur: 'Agricole', domaine: 'Agriculture', projets: 5, color: '#0088FE' },
    { secteur: 'Transport', domaine: 'Transport commun', projets: 3, color: '#00C49F' },
    { secteur: 'Santé', domaine: 'Pharmatie', projets: 12, color: '#00C49F' },
    { secteur: 'Numérique', domaine: 'Infrographie', projets: 4, color: '#FFBB28' },
    { secteur: 'Commerce', domaine: 'Commerce général', projets: 6, color: '#AF19FF' },
    { secteur: 'Hummanitaire', domaine: 'Aide sociale', projets: 7, color: 'green' },
];

const Charts = () => {
    return (
        <div className='w-100 col d-flex'>
            <div className='col'>
                <div className='col-md-3'>
                    <LineChart width={350} height={250} data={dataFactures}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Line type="monotone" dataKey="transaction" stroke="#ffc658" />
                    </LineChart>
                </div>
                <div className='col-md-6'>
                    <PieChart width={350} height={250}>
                        <Pie data={data} dataKey="ventes" nameKey="name" cx="50%" cy="50%" outerRadius={50} fill="#8884d8" >
                            {
                                data.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={entry.color} />
                                ))
                            }
                        </Pie>
                        <Tooltip contentStyle={{ color: 'red', backgroundColor: '#FF8042' }} />
                        <Legend />
                    </PieChart>
                </div>
            </div>
            <div className='col'>
            <div className='col-md-3 mt-3'>
                    <BarChart width={350} height={250} data={dataGroupe}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="groupe" fill="#ffc658" />
                    </BarChart>
                </div>
                <div className=' co-md-3'>
                    <PieChart width={350} height={250}>
                        <Pie
                            data={dataProjet}
                            dataKey="projets"
                            nameKey="secteur"
                            cx="50%"
                            cy="50%"
                            outerRadius={50}
                            fill="#8884d8"
                            label={(entry) => `${entry.secteur}: ${entry.projets}`}
                        >
                            {dataProjet.map((entry, index) => (
                                <Cell key={`cell-${index}`} fill={entry.color} />
                            ))}
                        </Pie>
                        <Tooltip />
                        <Legend />
                    </PieChart>
                    <Legend />
                </div>
            </div>
        </div>
    );
};

export default Charts;
