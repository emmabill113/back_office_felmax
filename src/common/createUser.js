import React from "react"
import { useState } from "react";

const CreateUser=()=>{
    const [isCreate, setIsCreate] = useState(false)

return(
<div className=" p-3 bg-body rounded shadow-sm">
    <h6 className="pb-2 mb-0 titre">Créer un utilisateur</h6>
    <section id="forms">
        <article className="my-3" id="validation">
            <div>
                <div className="bd-example">
                    <form className="row justify-content-center g-3">
                        <div className="col-md-4">
                            <label htmlFor="validationServer01" className="form-label">Nom</label>
                            <input type="text" className="form-control " id="validationServer01" required="" />
                        </div>
                        <div className="col-md-3">
                            <label htmlFor="validationServer02" className="form-label">Prénom</label>
                            <input type="text" className="form-control" id="validationServer02" required="" />
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="validationServerUsername" className="form-label">e-mail</label>
                            <div className="input-group has-validation">
                                <input type="email" className="form-control" id="validationServerUsername" aria-describedby="inputGroupPrepend3" required="" />
                            </div>
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="validationServer03" className="form-label">Ville</label>
                            <input type="text" className="form-control " id="validationServer03" required="" />
                        </div>
                        <div className="col-md-3">
                            <label htmlFor="validationServer03" className="form-label">Quartier</label>
                            <input type="text" className="form-control " id="validationServer03" required="" />
                        </div>

                        <div className="col-md-4">
                            <label htmlFor="validationServer05" className="form-label">Numéro de télephone</label>
                            <input type="text" className="form-control " id="validationServer05" required="" />
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="validationServer05" className="form-label">Mot de passe</label>
                            <input type="text" className="form-control " id="validationServer05" required="" />
                        </div>
                        <div className="col-md-3">
                            <label htmlFor="validationServer05" className="form-label">Confirmer le mot de passe</label>
                            <input type="text" className="form-control " id="validationServer05" required="" />
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="validationServer05" className="form-label">Adresse</label>
                            <input type="text" className="form-control " id="validationServer05" required="" />
                        </div>
                        <div className="col-11">
                            <div className="form-check">
                                <input className="form-check-input " type="checkbox" id="invalidCheck3" required="" />
                                <label className="form-check-label" htmlFor="invalidCheck3">
                                    vous accepter les termes et conditions</label>
                            </div>
                        </div>
                        <div className="row mt-4 justify-content-center">
                            <div className="col-4">
                                <button onClick={() => setIsCreate(false)} className="btn-primary" type="submit">Annuler</button>
                            </div>
                            <div className="col-4">
                                <button onClick={() => setIsCreate(false)} className="btn-primary" type="submit">Créer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </article>
    </section>

    {/* */}
</div>);
}
export default CreateUser;