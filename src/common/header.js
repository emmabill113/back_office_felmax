import React from "react";
import "./index.css"
import { Link } from "react-router-dom";


const Header = () =>{
    
        return (<nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top border-bottom">
            <div className="container-fluid">
                <Link className="navbar-toggler" type="Link" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span className="navbar-toggler-icon"></span></Link>
                <nav class="navbar bg-light">
                    <div class="container-fluid">
                        <form class="d-flex" role="search">
                            <input class="form-control me-2" type="search" placeholder="Recherche globale" aria-label="Search" />
                            <Link class="btn-seach" type="submit">Recherche</Link>
                        </form>
                    </div>
                </nav>
            </div>
            <div className="col text-right">
                <Link className="btn  low-height-btn">
                    <i className="fa  incon  fa-spinner fa-spin"></i>
                </Link>
            </div>
            <div className="col text-right">
                <Link to="/" className="btn  low-height-btn">
                    <i className="fa  incon fa-line-chart"></i>
                </Link>
            </div>
            <div className="col text-right">
                <Link className="btn  low-height-btn">
                    <i className="fa  incon fa-group"></i>
                </Link>
            </div>
            <div className="col text-right">
                <Link className="btn  low-height-btn">
                    <i className="fa  incon fa-envelope"></i>
                </Link>
            </div>
            <div className="col text-right">
                <Link className="btn  low-height-btn">
                    <i className="fa  incon fa-bell"></i>
                </Link>
            </div>
            <div className="col text-right">
                <Link className="btn  " to="/profile">
                    <i className="fa  incon fa-cog"></i>
                </Link>
            </div>
        </nav>);
}

export default Header;