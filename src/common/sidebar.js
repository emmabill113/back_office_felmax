import React from "react";
import 'react-perfect-scrollbar/dist/css/styles.css';
import PerfectScrollbar from 'react-perfect-scrollbar'
import { Link } from 'react-router-dom';
import profile from "../assets/images/profile.jpg"
import "./index.css";
import { connect, useDispatch } from 'react-redux';
import {logout} from '../redux/auth/actions';


const Sidebar = ({userInfo}) => {
    
    const dispatch = useDispatch();

   

        return( <div className="border-end sidenav" id="sidebar-wrapper">
            <div className="sidebar-heading logo border-bottom">
                <Link to="/">
                    <img className="imge"  alt="Alt content" src={require('./../assets/images/logo.png')} />
                </Link>
            </div>
            <PerfectScrollbar className="sidebar-items">
                <ul className="list-unstyled ">
                    <li className="mb-3">
                        <Link tag="a" className="drow" to="/">
                            <i className="fa fa-dashboard"></i> Dashboard
                        </Link>
                    </li>
                    <li className="mb-3">
                        <Link tag="a" className="drow" to="/transaction">
                            <i className="fa fa-file-o"> </i> Transactions
                        </Link>
                    </li>
                    <li className="mb-3">
                        <Link tag="a" className="drow" to="/tontine">
                        <i className="fa fa-group" aria-hidden="true"> </i> Tontines
                        </Link>
                    </li>
                    <li className="mb-3">
                        <Link tag="a" className="drow" to="/typography">
                            <i className="fa fa-file"></i> Projets
                        </Link>
                    </li>
                    <li className="mb-3">
                        <Link tag="a" className="drow" to="/list-user">
                         <i  className="fa drow fa-user"> </i> Utilisateurs
                        </Link>
                    </li>
                   
                </ul>
            </PerfectScrollbar>
            <div className="dropdown fixed-bottom-dropdown">
                <a href="#" className="d-flex align-items-center text-decoration-none dropdown-toggle" id="dropdownUser2" data-bs-toggle="dropdown" aria-expanded="false">
                    <img src={userInfo?.picture} alt="" width="32" height="32" className="rounded-circle me-2" />
                    <span>{userInfo?.name}</span>
                </a>
                <ul className="dropdown-menu text-small shadow" aria-labelledby="dropdownUser2">
                    <li><Link className="dropdown-item" to="/profile"><i className="fa fa-user-circle" aria-hidden="true"></i> Profile</Link></li>
                    <li><hr className="dropdown-divider" /></li>
                    <li><Link onClick={()=>dispatch(logout())} className="dropdown-item" ><i className="fa fa-sign-out" aria-hidden="true"></i> Se déconnecter</Link></li>
                </ul>
            </div>
        </div>
        )
}

const mapStateToProps = ({ AuthReducer }) => ({
    userInfo: AuthReducer.userInfo,
  });

export default connect(mapStateToProps)(Sidebar);